include(FindPackageHandleStandardArgs)

find_path(ACL_INCLUDE_DIR NAMES "sys/acl.h" "acl/libacl.h")

find_library(ACL_LIBRARY NAMES acl)

if (ACL_INCLUDE_DIR AND ACL_LIBRARY)   
    find_package_handle_standard_args(
        ACL
        REQUIRED_VARS ACL_LIBRARY ACL_INCLUDE_DIR
    )
    
    if(ACL_FOUND)
        set(ACL_LIBRARIES ${ACL_LIBRARY})
        set(ACL_INCLUDE_DIRS ${ACL_INCLUDE_DIR})
        if(NOT TARGET ACL::ACL)
            add_library(ACL::ACL UNKNOWN IMPORTED)
            set_target_properties(ACL::ACL PROPERTIES INTERFACE_INCLUDE_DIRECTORIES "${ACL_INCLUDE_DIRS}")
            set_property(TARGET ACL::ACL APPEND PROPERTY IMPORTED_LOCATION "${ACL_LIBRARY}")
        endif()
    endif()

    mark_as_advanced(ACL_INCLUDE_DIRS ACL_LIBRARIES ACL_INCLUDE_DIR ACL_LIBRARY)
endif()
