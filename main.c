#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <errno.h>
#include <assert.h>
#include <acl/libacl.h>
#include <sys/acl.h>

static int acl_find_uid(acl_t acl, uid_t uid, acl_entry_t *entry)
{
    acl_entry_t i;
    int r;

    assert(acl);
    assert(entry);

    for (r = acl_get_entry(acl, ACL_FIRST_ENTRY, &i);
         r > 0;
         r = acl_get_entry(acl, ACL_NEXT_ENTRY, &i)) {

        acl_tag_t tag;
        uid_t *u;
        bool b;

        if (acl_get_tag_type(i, &tag) < 0)
            return -errno;

        if (tag != ACL_USER)
            continue;

        u = acl_get_qualifier(i);
        if (!u)
            return -errno;

        b = *u == uid;
        acl_free(u);

        if (b) {
            *entry = i;
            return 1;
        }
    }
    if (r < 0)
        return -errno;

    return 0;
}

static int devnode_acl(const char *path,
                //bool flush, // always false
                bool del, uid_t old_uid,
                bool add, uid_t new_uid)
{
    acl_t acl;
    int r = 0;
    bool changed = false;

    acl = acl_get_file(path, ACL_TYPE_ACCESS);
    if (!acl)
        return -errno;

    /*if (flush) {

        r = flush_acl(acl);
        if (r < 0)
            goto finish;
        if (r > 0)
            changed = true;

    } else*/ if (del && old_uid > 0) {
        acl_entry_t entry;

        r = acl_find_uid(acl, old_uid, &entry);
        if (r < 0)
            goto finish;

        if (r > 0) {
            if (acl_delete_entry(acl, entry) < 0) {
                r = -errno;
                goto finish;
            }

            changed = true;
        }
    }

    if (add && new_uid > 0) {
        acl_entry_t entry;
        acl_permset_t permset;
        int rd, wt;

        r = acl_find_uid(acl, new_uid, &entry);
        if (r < 0)
            goto finish;

        if (r == 0) {
            if (acl_create_entry(&acl, &entry) < 0) {
                r = -errno;
                goto finish;
            }

            if (acl_set_tag_type(entry, ACL_USER) < 0 ||
                acl_set_qualifier(entry, &new_uid) < 0) {
                r = -errno;
                goto finish;
            }
        }

        if (acl_get_permset(entry, &permset) < 0) {
            r = -errno;
            goto finish;
        }

        rd = acl_get_perm(permset, ACL_READ);
        if (rd < 0) {
            r = -errno;
            goto finish;
        }

        wt = acl_get_perm(permset, ACL_WRITE);
        if (wt < 0) {
            r = -errno;
            goto finish;
        }

        if (!rd || !wt) {

            if (acl_add_perm(permset, ACL_READ|ACL_WRITE) < 0) {
                r = -errno;
                goto finish;
            }

            changed = true;
        }
    }

    if (!changed)
        goto finish;

    if (acl_calc_mask(&acl) < 0) {
        r = -errno;
        goto finish;
    }

    if (acl_set_file(path, ACL_TYPE_ACCESS, acl) < 0) {
        r = -errno;
        goto finish;
    }

    r = 0;

finish:
    acl_free(acl);

    return r;
}

int main(int argc, char *argv[])
{
    int r = 0;
    uid_t old_uid = 0;
    uid_t new_uid = 0;

    if (argc < 4) {
        fprintf(stderr, "usage: %s <filename> <old_uid> <new_uid>\n", argv[0]);
        fprintf(stderr, "  old_uid maybe zero if removing old user ACL is not required\n");
        fprintf(stderr, "  new_uid maybe zero if adding new user ACL is not required\n");
        exit(EXIT_FAILURE);
    }

    sscanf(argv[2], "%u", &old_uid);
    sscanf(argv[3], "%u", &new_uid);

    printf("Changing ACL for: %s (%u -> %u)...\n", argv[1], old_uid, new_uid);
    fflush(stdout);

    r = devnode_acl(argv[1],
                    (old_uid > 0) ? true : false, old_uid,
                    (new_uid > 0) ? true : false, new_uid);

    printf("devnode_acl() ret = %d, errno = %d\n", r, errno);

    return EXIT_SUCCESS;
}
