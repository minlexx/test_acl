= test_acl =

Created to test kernel crash that elogind caused on Samsung Galaxy S5 phone:
https://gitlab.com/postmarketOS/pmaports/issues/274

Usage: `sudo strace test_acl /dev/v4l-subdev10 0 10000` or smth like that